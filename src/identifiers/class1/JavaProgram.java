package identifiers.class1;

public class JavaProgram {

	static byte b1;
	static short s1;
	static int i1;
	static char c1;
	static long l1;
	static double d1;
	static float f1;
	static boolean bl1;
	
	public static void main(String[] args) {
		
		/*
		 
		 Print the default value of all data type
		 Check the console after execution.
		 
		 */
			
		System.out.println(b1);
		System.out.println(s1);
		System.out.println(i1);
		System.out.println(c1);
		System.out.println(l1);
		System.out.println(d1);
		System.out.println(f1);
		System.out.println(bl1);

		//System.out.println("Hello World"); 
		//System.out.println(123.23);
		
		String str = "Hello World";		
		System.out.println(str);
		
		short s = 1234;
		
		System.out.println(s);
		
		int x = 123248;
		
		System.out.println(x);
		
		byte b = 1;
		
		System.out.println(b);
		
		long l = 1234566612323l;
		
		System.out.println(l);

		double d = 343434343434d;

		System.out.println(d);
		
		//if we want to print it without exponential
		
		System.out.printf("dexp: %.0f\n", d);
		System.out.printf("dexp: %.2f\n", d);
		
		float f = 12.22f;
		
		System.out.println(f);
		
		char c = 'f';
		
		System.out.println(c);
		
		boolean val = false;
		
		System.out.println(val);
		
		String str1 = "false";
		
		System.out.println(str1);
		
	}
}
